package main

import (
    "testing"
)

func TestDiskCreation(t *testing.T) {
    _, err := newDisk("foo", 9991, "Foobar Studio", "bar")
    if err != nil {
        t.Error(err)
    }
}