package main

import (
	"errors"
	"fmt"
	"github.com/TwiN/go-color"
	"log"
	"strconv"
)

type disk struct {
	title          string
	trucatedMemory int
	memoryUnit     string
	console        string
	romtype        string
	path 		   string
}

func main() {
	log.Println(color.Ize(color.Green, "Welcome"))
	log.Println(color.Ize(color.Green, "Starting Initial Setup"))
}

func newDisk(title string, memoryKib float64, console string, romType string, path string) (disk, error) {
	if memoryKib <= 0 {
			return disk{}, errors.New("Invalid memory value")
	}
	var r string
	if memoryKib < 1024 {
			r = "Kib"
	} else if memoryKib < 1024*1024 {
			r = "Mib"
	} else if memoryKib < 1024*1024*1024 {
			r = "Gib"
	} else if memoryKib < 1024*1024*1024*1024 {
			r = "Tib"
	} else {
	r = "Too Large File"
	}
	t, err := strconv.Atoi(fmt.Sprintf("%.0f", memoryKib))
	if err != nil {
		return disk{}, errors.New(err)
	}
	d := disk{
			title:          title,
			trucatedMemory: t,
			memoryUnit:     r,
			console:        console,
			romtype:        romType,
			path: path,
	}
	return d, nil
}

func mountDisk() {

}
